package com.te.pms.service;

import java.util.List;

import com.te.pms.dto.PostMetaDto;

public interface PostMetaService {

	public PostMetaDto addPostMeta(PostMetaDto postMetaDto);

	public List<PostMetaDto> getPostMeta(Long postId);

	public PostMetaDto updatePostMetaDto(PostMetaDto postMetaDto);

	public PostMetaDto deletePostMetaDto(Long postMetaId);

}
