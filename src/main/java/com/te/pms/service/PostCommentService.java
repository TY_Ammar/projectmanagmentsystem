package com.te.pms.service;

import java.util.List;

import com.te.pms.dto.PostCommentDto;

public interface PostCommentService {
	public PostCommentDto addPostComment(PostCommentDto postCommentDto);

	public List<PostCommentDto> getPostComment(Long postId);

	public PostCommentDto updatePostComment(PostCommentDto postCommentDto);

	public PostCommentDto deletePostComment(Long postCommentId);

}
