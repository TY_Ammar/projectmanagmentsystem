package com.te.pms.service;

import java.util.List;

import com.te.pms.dto.TagDto;

public interface TagService {
	public TagDto addTag(TagDto tagDto);

	public List<TagDto> getTag(Long postId);

	public TagDto updateTag(TagDto tagDto);

	public TagDto deleteTag(Long tagId);

}
