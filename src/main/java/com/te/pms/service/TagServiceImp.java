package com.te.pms.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.te.pms.dao.PostDao;
import com.te.pms.dao.TagDao;
import com.te.pms.dto.TagDto;
import com.te.pms.entity.Post;
import com.te.pms.entity.Tag;
import com.te.pms.exception.UserNotFoundException;

@Service
public class TagServiceImp implements TagService {
	@Autowired
	private PostDao postDao;

	@Autowired
	private TagDao tagDao;

	@Override
	public TagDto addTag(TagDto tagDto) {
		Post post = postDao.findById(tagDto.getPostId())
				.orElseThrow(() -> new UserNotFoundException("invalid post id"));
		Tag tag = new Tag();
		BeanUtils.copyProperties(tagDto, tag);
		tag.setPostList(List.of(post));
		Tag saveTag = tagDao.save(tag);
		return tagDto;
	}

	@Override
	public List<TagDto> getTag(Long postId) {
		Optional<Post> findById = postDao.findById(postId);
		if (findById.isEmpty()) {
			throw new UserNotFoundException("invalid id");

		}
		Post post = findById.get();
		List<Tag> tagList = post.getTagList();
		List<TagDto> tagDtoList = new ArrayList<>();
		tagList.forEach(i -> {
			TagDto tagDto = new TagDto();
			BeanUtils.copyProperties(i, tagDto);
			tagDtoList.add(tagDto);

		});
		return tagDtoList;

	}

	@Override
	public TagDto updateTag(TagDto tagDto) {
		Optional<Tag> findById = tagDao.findById(tagDto.getTagId());
		if (findById.isPresent()) {
			BeanUtils.copyProperties(tagDto, findById.get());
			tagDao.save(findById.get());
			BeanUtils.copyProperties(findById.get(), tagDto);
			return tagDto;

		}
		throw new UserNotFoundException("invalid id");
	}

	@Override
	public TagDto deleteTag(Long tagId) {
		Optional<Tag> findById = tagDao.findById(tagId);
		if (findById.isPresent()) {
			tagDao.deleteById(tagId);
			return new TagDto();

		}
		throw new UserNotFoundException("id not present");
	}

}
