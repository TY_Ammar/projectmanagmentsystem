package com.te.pms.service;

import java.util.List;

import com.te.pms.dto.PostDto;

public interface PostService {

	public PostDto addPost(PostDto postDto);

	public List<PostDto> getPost(Long userId);

	public PostDto updatePostDto(PostDto postDto);

	public PostDto deletePostDto(Long postId);

}
