package com.te.pms.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.te.pms.dao.CategoryDao;
import com.te.pms.dao.PostDao;
import com.te.pms.dao.UserDao;
import com.te.pms.dto.PostDto;
import com.te.pms.entity.Post;
import com.te.pms.entity.User;
import com.te.pms.exception.UserNotFoundException;

@Service
public class PostServiceImp implements PostService {

	@Autowired
	private UserDao userDao;

	@Autowired
	private PostDao postDao;

	@Override
	public PostDto addPost(PostDto postDto) {
		User orElseThrow = userDao.findByUserId(postDto.getUserId()).orElseThrow(() -> new UserNotFoundException("no user "));
		Post post = new Post();
		post.setUser(orElseThrow);
		BeanUtils.copyProperties(postDto, post);
		Post savePost = postDao.save(post);
		PostDto postDto2 = new PostDto();
		BeanUtils.copyProperties(savePost, postDto2);
		return postDto2;
	}

	@Override
	public List<PostDto> getPost(Long userId) {
		Optional<User> findById = userDao.findById(userId);
		if (findById.isEmpty()) {
			throw new UserNotFoundException("invalid user id");
		}
		User user = findById.get();
		List<Post> postList = user.getPostList();
		List<PostDto> postDtoList = new ArrayList<>();
		postList.forEach(i -> {
			PostDto postDto = new PostDto();
			BeanUtils.copyProperties(i, postDto);
			postDtoList.add(postDto);
		});

		return postDtoList;

	}

	@Override
	public PostDto updatePostDto(PostDto postDto) {
		Optional<Post> findById = postDao.findById(postDto.getPostId());
		if (findById.isPresent()) {
			BeanUtils.copyProperties(postDto, findById.get());
			postDao.save(findById.get());
			BeanUtils.copyProperties(findById.get(), postDto);
			return postDto;
		}
		throw new UserNotFoundException("post updation fialed");
	}

	@Override
	public PostDto deletePostDto(Long postId) {
		Optional<Post> findById = postDao.findById(postId);
		if (findById.isPresent()) {
			postDao.deleteById(postId);
			return new PostDto();
		}
		throw new UserNotFoundException("id not present");
	}

}
