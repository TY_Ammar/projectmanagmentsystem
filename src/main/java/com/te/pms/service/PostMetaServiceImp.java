package com.te.pms.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.te.pms.dao.PostDao;
import com.te.pms.dao.PostMetaDao;
import com.te.pms.dto.PostMetaDto;
import com.te.pms.entity.Post;
import com.te.pms.entity.PostMeta;
import com.te.pms.exception.UserNotFoundException;

@Service
public class PostMetaServiceImp implements PostMetaService {

	@Autowired
	private PostDao postDao;

	@Autowired
	private PostMetaDao postMetaDao;

	@Override
	public PostMetaDto addPostMeta(PostMetaDto postMetaDto) {
		Post orElseThrow = postDao.findByPostId(postMetaDto.getPostId())
				.orElseThrow(() -> new UserNotFoundException("invalid post is"));
		PostMeta postMeta = new PostMeta();
		postMeta.setPost(orElseThrow);
		BeanUtils.copyProperties(postMetaDto, postMeta);
		PostMeta savePostMeta = postMetaDao.save(postMeta);
		PostMetaDto postMetaDto2 = new PostMetaDto();
		BeanUtils.copyProperties(savePostMeta, postMetaDto2);
		return postMetaDto2;
	}

	@Override
	public List<PostMetaDto> getPostMeta(Long postId) {
		Optional<Post> findById = postDao.findById(postId);
		if (findById.isEmpty()) {
			throw new UserNotFoundException("invalid post id");
		}
		Post post = findById.get();
		List<PostMeta> postMetaList = post.getPostMetaList();
		List<PostMetaDto> postMetaDtoList = new ArrayList<>();
		postMetaList.forEach(i -> {
			PostMetaDto postMetaDto = new PostMetaDto();
			BeanUtils.copyProperties(i, postMetaDto);
			postMetaDtoList.add(postMetaDto);

		});

		return postMetaDtoList;
	}

	@Override
	public PostMetaDto updatePostMetaDto(PostMetaDto postMetaDto) {
		Optional<PostMeta> findById = postMetaDao.findById(postMetaDto.getPostMetaId());
		if (findById.isPresent()) {
			BeanUtils.copyProperties(postMetaDto, findById.get());
			postMetaDao.save(findById.get());
			BeanUtils.copyProperties(findById.get(), postMetaDto);
			return postMetaDto;

		}
		throw new UserNotFoundException("invalid id");
	}

	@Override
	public PostMetaDto deletePostMetaDto(Long postMetaId) {
		Optional<PostMeta> findById = postMetaDao.findById(postMetaId);
		if (findById.isPresent()) {
			postMetaDao.deleteById(postMetaId);
			return new PostMetaDto();

		}
		throw new UserNotFoundException("invalid id");
	}

}
