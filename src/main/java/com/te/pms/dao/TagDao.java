package com.te.pms.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.te.pms.entity.Post;
import com.te.pms.entity.Tag;

public interface TagDao extends JpaRepository<Tag, Long> {
	Tag save(Optional<Post> findByPostId);

}
