package com.te.pms.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PostCommentDto {
	private Long postCommentId;
	private Long parentId;
	private String title;
	private String published;
	private String content;
	private Long postId;
	private Long userId;

}
